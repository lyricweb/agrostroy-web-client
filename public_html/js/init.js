"use strict";

var App = function(){

  function init(){
    svg4everybody()
    yaMapContacts()
  }

  function isTouch(){
    return ('ontouchstart' in document.documentElement) ? true : false
  }

  /*******   yandex карта в контактах    *******/
  function yaMapContacts() {
    var map = document.querySelector('.map')
    console.log(map);
    if (!map) {return}
    loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=b539e2d3-1424-4e8a-9d72-a12b47ec8988", function(){
      ymaps.ready(function () {
        console.log(1);
        var myMap = new ymaps.Map('map1', {
          center: [51.734584, 39.062396],
          zoom: 15,
          controls: []
        })
        var myPlacemark = new ymaps.Placemark([51.734584, 39.062396], {
          hintContent: 'Адрес',
          balloonContent:  '<div class="balloon-content"><div class="balloon-info"><h6>Совхоз Байгора</h6><button class="btn btn-main">Подробнее</button></div><div class="balloon-img" style="background-image:url(../images/projects/prj-1.jpg);"></div></div>'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        })
        myMap.geoObjects.add(myPlacemark)
      })
    })
  }

  function loadScript(url, callback){
    var script = document.createElement("script")
    script.type = "text/javascript"
    script.async = true
    if (script.readyState){ 
      script.onreadystatechange = function(){
        if (script.readyState === "loaded" ||
            script.readyState === "complete"){
          script.onreadystatechange = null
          callback()
        }
      }
    } else {
      script.onload = function(){
        callback()
      }
    }
    script.src = url
    document.getElementsByTagName("head")[0].appendChild(script)
  }
  
  document.addEventListener("DOMContentLoaded", function(){
    init()
  }, false)

  return{
    isTouch: isTouch,
    loadScript: loadScript,
    yaMapContacts: yaMapContacts,
  }
}()

