(function () {
  'use strict';
  window.addEventListener('load', function () {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


$(document).ready(function(){
  $('.main__slider').slick({
    slidesToShow: 1,
    arrows: false,
    autoplay: true,
    slidesToScroll: 1,
    dots: true
  })
  $('.news__slider').slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          infinite: false,
          variableWidth: true,
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToScroll: 1
        }
      }
    ],
    prevArrow: `<button class="btn slider-btn left-btn">
                  <svg viewBox="0 0 40 36" width="20" height="17">
                    <use xlink:href="../svg/icons.svg#arrow-left"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="btn slider-btn right-btn">
                  <svg viewBox="0 0 40 36" width="20" height="17">
                    <use xlink:href="../svg/icons.svg#arrow-right"/>
                  </svg>
                </button>`
  })
  $('.products-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    prevArrow: `<button class="btn slider-btn left-btn">
                  <svg viewBox="0 0 40 36" width="40" height="36">
                    <use xlink:href="../svg/icons.svg#arrow-left"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="btn slider-btn right-btn">
                  <svg viewBox="0 0 40 36" width="40" height="36">
                    <use xlink:href="../svg/icons.svg#arrow-right"/>
                  </svg>
                </button>`
  })
  $('.detail-project__gallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    prevArrow: `<button class="btn slider-btn left-btn">
                  <svg viewBox="0 0 40 36" width="40" height="36">
                    <use xlink:href="../svg/icons.svg#arrow-left"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="btn slider-btn right-btn">
                  <svg viewBox="0 0 40 36" width="40" height="36">
                    <use xlink:href="../svg/icons.svg#arrow-right"/>
                  </svg>
                </button>`
  })

  if ($(window).width() <= '1199') {
    $('.detail-product .nav-tabs').slick({
      infinite: false,
      variableWidth: true,
      arrows: false,
      slidesToScroll: 1
    })
  }

  if ($(window).width() <= '768') {
    $('.main-menu__list').slick({
      infinite: false,
      variableWidth: true,
      arrows: false,
      centerPadding: '40px',
      slidesToScroll: 1
    })
  }

  $('.nav-item a').on('click', function (e) {
    e.preventDefault();
    $('.nav-link').removeClass('active');
    $(this).tab('show');
  });

  $('.form-control').focus(function(){
    $(this).parents('.form-group').addClass('focus-control')
  });

  $('.form-control').blur(function(){
    $(this).parents('.form-group').removeClass('focus-control')
  });

  $('.hamburger').click(function() {
    $('body').toggleClass('page-overflow')
    $(this).toggleClass('active')
    $('.wrap-nav-menu').toggleClass('open')
  })

  $('.lang-block').click(function() {
    $(this).toggleClass('active')
  })

  $('.main-circle.search').click(function() {
    $('.search__block').addClass('show')
  })

  $(document).mouseup(function (e) {
    var container = $('.main-circle.search')
    if (container.has(e.target).length === 0){
      $('.search__block').removeClass('show')
    }
  });

  $('.callback__info').click(function() {
    $('body').addClass('page-overflow')
    $('.callback-popup').fadeIn()
  })

  $('.popup__close, .close-answer').click(function() {
    $('body').removeClass('page-overflow')
    $(this).parents('.popup').fadeOut()
  })

  $('.call-close').click(function() {
    $('.callback').hide()
  })

  $('.nav-menu__item').mouseenter(function() {
    $(this).find('.dropdown-menu').addClass('open')
  })
  $('.nav-menu__item').mouseleave(function() {
    $(this).find('.dropdown-menu').removeClass('open')
  })
  $('.scroll-to-up').click(function() {
    $('html, body').animate({ scrollTop: 0 }, 'slow')
    return false
  })
})
